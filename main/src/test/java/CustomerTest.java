import org.junit.Assert;
import org.junit.Test;

public class CustomerTest {

    @Test
    public void CreateCustomer() {
        Customer customer = new Customer("Nico");
        Assert.assertEquals("Nico", customer.getName());
    }
    @Test
    public void statementChildrens(){
        Movie movie = new Movie("Frozen", Movie.CHILDRENS);
        Rental rental= new Rental(movie, 2);
        Customer customer = new Customer("Nico");
        customer.addRental(rental);
        Assert.assertEquals("Rental Record for Nico\n" + "\tTitle\t\tDays\tAmount\n" + "\tFrozen\t\t2\t1.5\n" + "Amount owed is 1.5\n"
                + "You earned 1 frequent renter points", customer.statement());
    }
    @Test
    public void statementReleaseFrequentPoints(){
        Movie movie = new Movie("Iron Man", Movie.NEW_RELEASE);
        Rental rental= new Rental(movie, 30);
        Customer customer = new Customer("Nico");
        customer.addRental(rental);
        Assert.assertEquals("Rental Record for Nico\n" + "\tTitle\t\tDays\tAmount\n" + "\tIron Man\t\t30\t90.0\n" + "Amount owed is 90.0\n"
                + "You earned 2 frequent renter points", customer.statement());
    }

}