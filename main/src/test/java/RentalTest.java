import org.junit.Assert;
import org.junit.Test;

public class RentalTest {

    @Test
    public void IronMan14DayRented() {
        Movie movie = new Movie("Iron Man", Movie.REGULAR);
        Rental rental = new Rental(movie, 14);
        Assert.assertEquals(14, rental.getDaysRented());
        Assert.assertEquals(movie, rental.getMovie());
    }
}