import org.junit.Assert;
import org.junit.Test;

public class MovieTest {

    @Test
    public void CreateMovie() {
        Movie movie = new Movie("Iron Man", Movie.REGULAR);
        Assert.assertEquals(Movie.REGULAR, movie.getPriceCode());
        Assert.assertEquals("Iron Man", movie.getTitle());
    }
    @Test
    public void AlterMovie() {
        Movie movie = new Movie("Iron Man", Movie.REGULAR);
        movie.setPriceCode(Movie.CHILDRENS);
        Assert.assertEquals(Movie.CHILDRENS, movie.getPriceCode());
    }
}